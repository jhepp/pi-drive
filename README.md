This Node.js application operates in linux and allows basic mount/unmount and 
upload/download of files to USB drives.

Each partition on a drive can be mounted and unmounted independently.

Uploads and downloads are all performed within a pidrive directory on the
USB drive. The directory is created if it does not already exist.

This application depends on the pmount/pumount tool which allows users to 
mount and unmount USB drives.

More information can be found on www.heppic.com