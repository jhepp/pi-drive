function pug_escape(e){var a=""+e,t=pug_match_html.exec(a);if(!t)return e;var r,c,n,s="";for(r=t.index,c=0;r<a.length;r++){switch(a.charCodeAt(r)){case 34:n="&quot;";break;case 38:n="&amp;";break;case 60:n="&lt;";break;case 62:n="&gt;";break;default:continue}c!==r&&(s+=a.substring(c,r)),c=r+1,s+=n}return c!==r?s+a.substring(c,r):s}
var pug_match_html=/["&<>]/;
function pug_rethrow(n,e,r,t){if(!(n instanceof Error))throw n;if(!("undefined"==typeof window&&e||t))throw n.message+=" on line "+r,n;try{t=t||require("fs").readFileSync(e,"utf8")}catch(e){pug_rethrow(n,null,r)}var i=3,a=t.split("\n"),o=Math.max(r-i,0),h=Math.min(a.length,r+i),i=a.slice(o,h).map(function(n,e){var t=e+o+1;return(t==r?"  > ":"    ")+t+"| "+n}).join("\n");throw n.path=e,n.message=(e||"Pug")+":"+r+"\n"+i+"\n\n"+n.message,n}function driveSummary(locals) {var pug_html = "", pug_mixins = {}, pug_interp;var pug_debug_filename, pug_debug_line;try {;var locals_for_with = (locals || {});(function (model, vendor) {;pug_debug_line = 1;pug_debug_filename = "template\u002Fdrive_summary.pug";
pug_html = pug_html + "\u003Cdiv class=\"card m-3 bg-light\"\u003E";
;pug_debug_line = 2;pug_debug_filename = "template\u002Fdrive_summary.pug";
pug_html = pug_html + "\u003Cdiv class=\"card-header\"\u003E";
;pug_debug_line = 2;pug_debug_filename = "template\u002Fdrive_summary.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = vendor) ? "" : pug_interp));
;pug_debug_line = 2;pug_debug_filename = "template\u002Fdrive_summary.pug";
pug_html = pug_html + " ";
;pug_debug_line = 2;pug_debug_filename = "template\u002Fdrive_summary.pug";
pug_html = pug_html + (pug_escape(null == (pug_interp = model) ? "" : pug_interp)) + "\u003C\u002Fdiv\u003E";
;pug_debug_line = 3;pug_debug_filename = "template\u002Fdrive_summary.pug";
pug_html = pug_html + "\u003Cdiv class=\"card-body text-centered py-0\"\u003E";
;pug_debug_line = 4;pug_debug_filename = "template\u002Fdrive_summary.pug";
pug_html = pug_html + "\u003Cpre class=\"py-0 mb-0\" id=\"mounts\"\u003E\u003C\u002Fpre\u003E\u003C\u002Fdiv\u003E\u003C\u002Fdiv\u003E";}.call(this,"model" in locals_for_with?locals_for_with.model:typeof model!=="undefined"?model:undefined,"vendor" in locals_for_with?locals_for_with.vendor:typeof vendor!=="undefined"?vendor:undefined));} catch (err) {pug_rethrow(err, pug_debug_filename, pug_debug_line);};return pug_html;}