const express = require('express');
const fileUpload = require('express-fileupload');
const cors = require('cors');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const _ = require('lodash');
const fs = require('fs');
const { exec } = require('child_process');
const pug = require('pug');
const async = require('async');

const app = express();

const port = process.env.PORT || 8088;


//Generate scripts
console.log("Generating scripts...")
const pugMountSummary = pug.compileFileClient('template/mount_summary.pug',
  { name: 'mountSummary'});
fs.writeFileSync("gen/template_mount_summary.js", pugMountSummary);

const pugDriveSummary = pug.compileFileClient('template/drive_summary.pug',
  { name: 'driveSummary'});
fs.writeFileSync("gen/template_drive_summary.js", pugDriveSummary);

const pugDownloadModal = pug.compileFileClient('template/download_modal.pug',
  { name: 'downloadModal'});
fs.writeFileSync("gen/template_download_modal.js", pugDownloadModal);

const pugUploadModal = pug.compileFileClient('template/upload_modal.pug',
  { name: 'uploadModal'});
fs.writeFileSync("gen/template_upload_modal.js", pugUploadModal);

const pugIndex = pug.compileFile('template/index.pug');


app.use('/css', express.static(__dirname + '/css'));
app.use('/js', express.static(__dirname + '/js'));

app.use(fileUpload({
  createParentPath: true,
  useTempFiles: true,
  tempFileDir: './tmp',
  abortOnLimit: true
}));

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  limit: '10gb',
  extended: true
}));
app.use(morgan('dev'));

app.post('/v1/drives/dev/:dev', function(req, res) {
  console.log("Trying to mount "+req.params.dev);
  exec('pmount --umask 000 --noatime -w /dev/'+req.params.dev+' '+req.params.dev,
    (err, stdout, stderr) => {
      if(err) {
        console.error(err);
        return;
      }
      res.writeHead(200, {'Content-Type': 'text/html'});
      res.write("Mounted");
      res.end();
  });
});

app.get('/v1/mount/media/:dir', function(req, res) {
  console.log("Trying to read directory "+req.params.dir);
  var dir = '/media/'+req.params.dir+'/pidrive';
  var ls = [];

  if (fs.existsSync(dir)) {
    var files = fs.readdirSync(dir);
    console.log(files);
    ls.push({"path" : "pidrive", "files" : files});
  }
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write(JSON.stringify(ls));
    res.end();

});

app.get('/v1/mount/media/:dir1/:dir2/:file', function(req, res) {
  var file = '/media/'+req.params.dir1+'/'+req.params.dir2+'/'+req.params.file;
  res.download(file);
})

app.delete('/v1/mount/media/:dir', function(req, res) {
  console.log("Trying to unmount "+req.params.dir);
  exec('pumount /media/'+req.params.dir, (err, stdout, stderr) => {
    if(err) {
      console.error(err);
      return;
    }
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write("Unmounted");
    res.end();
  });
})

app.post('/v1/media/:dir/upload', function(req, res) {
  try {
    var dir = '/media/'+req.params.dir+'/pidrive';

    if (!req.files) {
      res.send({status: false, message: 'No file uploaded'});
    } else {
      if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
      }

      let file = req.files.filename;
      file.mv(dir+ '/' + file.name);

      res.send({
        status: true,
        message: 'File is uploaded',
        data: {
          name: file.name,
          mimetype: file.mimetype,
          size: file.size
        }
      });
    }
  } catch (err) {
    res.status(500).send(err);
  }

});

app.get('/v1/drives', async(req, res) => {
  exec('lsblk -J -p --output NAME,VENDOR,MODEL,FSTYPE,MOUNTPOINT', (err, stdout, stderr) => {
    if(err) {
      console.error(err);
      return;
    }
    console.log(stdout);

    var json = JSON.parse(stdout);
    var resp = {
      "drives": []
    }

    json["blockdevices"].forEach(function(blockdevice) {
      if(blockdevice["name"].startsWith("/dev/sd")) {
        drive = {
          "dev": blockdevice["name"],
          "vendor": blockdevice["vendor"],
          "model": blockdevice["model"],
          "mounts": []
        };
        blockdevice["children"].forEach(function(child) {
          drive.mounts.push({
            "point": child.mountpoint,
            "fs" : child.fstype,
            "dev": child.name
          });
        });
        resp.drives.push(drive);
      }
    });
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write(JSON.stringify(resp));
    res.end();
  });
});

app.get('/', async(req, res) => {
  res.writeHead(200, {'Content-Type': 'text/html'});
  res.write(pugIndex({
  }));
  res.end();
});


app.listen(port, () => {
  console.log(`Pi Drive is listening on port ${port}.`);
});
