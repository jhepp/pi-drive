function refresh() {
  $.ajax({
    type: 'GET',
    url: '/v1/drives',
    datatype: 'data'
  })
  .done(function(data) {
    resp = JSON.parse(data);
    $('#drives').empty()
    resp.drives.forEach(function(drv) {
      $('#drives').append(window.driveSummary({
        dev: drv.name,
        vendor: drv.vendor,
        model: drv.model,
      }));
      drv.mounts.forEach(function(mnt) {
        var mp = null;
        if (mnt.point != null)
          mp = mnt.point.split('/').pop();
        var devname = mnt.dev.split('/').pop();
        $('#mounts').append(window.mountSummary({
          name: devname,
          point: mp,
          fs: mnt.fs
        }));
        if (mp == null) {
          $('#mount-'+devname).removeClass('disabled');
          $('#unmount-'+devname).addClass('disabled');
          $('#upload-'+devname).addClass('disabled');
          $('#download-'+devname).addClass('disabled');
        } else {
          $('#mount-'+devname).addClass('disabled');
          $('#unmount-'+devname).removeClass('disabled');
          $('#upload-'+devname).removeClass('disabled');
          $('#download-'+devname).removeClass('disabled');
        }
      });
    })
  })
  .fail(function(jqXHR, textStatus, err) {
    console.log('AJAX error response:', textStatus);
  })
}

function mount(point) {
  $.ajax({
    type: 'POST',
    url: '/v1/drives/dev/'+point,
    datatype: 'data'
  })
  .done(function(data) {
    $('#mount-'+point).addClass('disabled');
    $('#unmount-'+point).removeClass('disabled');
    $('#upload-'+point).removeClass('disabled');
    $('#download-'+point).removeClass('disabled');
  })
  .fail(function(jqXHR, textStatus, err) {
    console.log('AJAX error response:', textStatus);
  })
}

function unmount(point) {
  $.ajax({
    type: 'DELETE',
    url: '/v1/mount/media/'+point,
    datatype: 'data'
  })
  .done(function(data) {
    $('#mount-'+point).removeClass('disabled');
    $('#unmount-'+point).addClass('disabled');
    $('#upload-'+point).addClass('disabled');
    $('#download-'+point).addClass('disabled');
  })
  .fail(function(jqXHR, textStatus, err) {
    console.log('AJAX error response:', textStatus);
  })
}

function ls(point) {
  $.ajax({
    type: 'GET',
    url: '/v1/mount/media/'+point,
    datatype: 'data'
  })
  .done(function(data) {
    $('#dlfiles').empty()
    resp = JSON.parse(data);
    resp.forEach(function(dir) {
      dir.files.forEach(function(file) {
        var href = '/v1/mount/media/'+point+'/'+dir.path+'/'+file;
        $('#dlfiles').append(
          "<a href=\""+href+"\" class=\"list-group-item list-group-item-action\" >"+file+"</li>"
        )
      })
    })
  })
  .fail(function(jqXHR, textStatus, err) {
    console.log('AJAX error response:', textStatus);
  })
}
