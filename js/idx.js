function refresh() {
  $.ajax({
    type: 'GET',
    url: '/drives',
    datatype: 'data'
  })
  .done(function(data) {
    drives = JSON.parse(data);
    $('#deck').empty()
    drives.forEach(function(drv) {
      $('#deck').append(window.driveCard({
        title: drv.path
      }));
    })
  })
  .fail(function(jqXHR, textStatus, err) {
    console.log('AJAX error response:', textStatus);
  })
}

function unmount(path) {
  $.ajax({
    type: 'DELETE',
    url: '/drives/'+path,
    datatype: 'data'
  })
  .done(function(data) {
    console.log('Unmounted');
  })
  .fail(function(jqXHR, textStatus, err) {
    console.log('AJAX error response:', textStatus);
  })
}
