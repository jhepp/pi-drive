$('#inputGroupFile').on('change', function() {
  var fileName = $(this).val().replace('C:\\fakepath\\',"");
  $(this).next('.custom-file-label').html(fileName);
})

$('#uploadModal').on('show.bs.modal', function(event) {
  var id = event.relatedTarget.id;
  var dev = id.substring(7);
  $('#uploadForm').attr("action", "/v1/media/"+dev+"/upload");
})
